#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include "Heartbeat.h"

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

using boost::shared_ptr;

#include <map>
#include <algorithm>
#include <Windows.h>
#include <tchar.h>
#include "HShield.h"
#include "beaengine/BeaEngine.h"
#include "AsmJit/Assembler.h"
#include "AsmJit/MemoryManager.h"

using namespace AsmJit;


DWORD imgStart, imgEnd;
unsigned char* memoryBackup;
std::map<DWORD, Int32> baseRegisterMap = std::map<DWORD, Int32>();


BOOL PatchCRC(DWORD addr, std::vector<std::pair<DWORD, DWORD>> ranges) {
	TCHAR buf[256];
	DISASM disasm;
	Int32 basereg;
	int instrLen = 0;

	memset(&disasm, 0, sizeof(disasm));
	disasm.EIP = addr;
	instrLen = Disasm(&disasm);

	if (instrLen == UNKNOWN_OPCODE) {
		_stprintf(buf, _T("Could not find disassemble instruction @ %08X"), disasm.EIP);
		OutputDebugString(buf);
		return FALSE;
	}

	sprintf((char*)buf, "%08X: %s", disasm.EIP, disasm.CompleteInstr);
	OutputDebugStringA((char*)buf);

	if (disasm.Argument1.ArgType == MEMORY_TYPE && disasm.Argument1.AccessMode == READ) {
		basereg = disasm.Argument1.Memory.BaseRegister;
	} else if (disasm.Argument2.ArgType == MEMORY_TYPE && disasm.Argument2.AccessMode == READ) {
		basereg = disasm.Argument2.Memory.BaseRegister;
	} else if (disasm.Argument3.ArgType == MEMORY_TYPE && disasm.Argument3.AccessMode == READ) {
		basereg = disasm.Argument3.Memory.BaseRegister;
	} else {
		_stprintf(buf, _T("Could not find a memory access argument @ %08X"), disasm.EIP);
		OutputDebugString(buf);
		return FALSE;
	}

	const GPReg* reg;
	switch (basereg) {
	case REG0: reg = &eax; break;
	case REG1: reg = &ecx; break;
	case REG2: reg = &edx; break;
	case REG3: reg = &ebx; break;
	case REG4: reg = &esp; break;
	case REG5: reg = &ebp; break;
	case REG6: reg = &esi; break;
	case REG7: reg = &edi; break;
	default:
		_stprintf(buf, _T("Could not decode a memory access argument @ %08X"), disasm.EIP);
		OutputDebugString(buf);
		return FALSE;
	}

	Assembler a;
	Label modifyReg = a.newLabel();
	Label skip = a.newLabel();

	a.pushfd();
	std::for_each(ranges.begin(), ranges.end(), [&](std::pair<DWORD, DWORD> range) {
		Label nextCheck = a.newLabel();
		a.cmp(*reg, range.first);
		a.jb(nextCheck);
		a.cmp(*reg, range.second);
		a.jb(modifyReg);
		a.bind(nextCheck);
	});

	a.jmp(skip);
	a.bind(modifyReg);
	a.sub(*reg, imgStart);
	a.add(*reg, (DWORD)memoryBackup);
	a.bind(skip);
	a.popfd();
	a.embed((LPVOID)disasm.EIP, instrLen);

	int overwrittenBytes = instrLen;
	while (overwrittenBytes < 5) {
		disasm.EIP += instrLen;
		instrLen = Disasm(&disasm);
		if (instrLen == UNKNOWN_OPCODE) {
			_stprintf(buf, _T("Could not find disassemble instruction @ %08X"), disasm.EIP);
			OutputDebugString(buf);
			return FALSE;
		}

		if (disasm.Instruction.AddrValue != 0) {
			Label execJmp = a.newLabel();
			Label skipJmp = a.newLabel();

			switch (disasm.Instruction.BranchType) {
			case JO: a.jo(execJmp); break;
			case JC: a.jc(execJmp); break;
			case JE: a.je(execJmp); break;
			case JA: a.ja(execJmp); break;
			case JS: a.js(execJmp); break;
			case JP: a.jp(execJmp); break;
			case JL: a.jl(execJmp); break;
			case JG: a.jg(execJmp); break;
			case JB: a.jb(execJmp); break;
			case JECXZ: a.cmp(ecx, 0); a.jz(execJmp); break;
			case JmpType: a.jmp(disasm.Instruction.AddrValue); break;
			case CallType: a.call(disasm.Instruction.AddrValue); break;
			case RetType: /* shouldn't happen?? */ break;
			case JNO: a.jno(execJmp); break;
			case JNC: a.jnc(execJmp); break;
			case JNE: a.jne(execJmp); break;
			case JNA: a.jna(execJmp); break;
			case JNS: a.jns(execJmp); break;
			case JNP: a.jnp(execJmp); break;
			case JNL: a.jnl(execJmp); break;
			case JNG: a.jng(execJmp); break;
			case JNB: a.jnb(execJmp); break;
			default:
				_stprintf(buf, _T("Could not decode branch instruction @ %08X"), disasm.EIP);
				OutputDebugString(buf);
				return FALSE;
			}

			a.jmp(skipJmp);
			a.bind(execJmp);
			a.jmp(disasm.Instruction.AddrValue);
			a.bind(skipJmp);
		} else {
			a.embed((LPVOID)disasm.EIP, instrLen);
		}

		overwrittenBytes += instrLen;
	}

	disasm.EIP += instrLen;
	a.jmp(disasm.EIP);

	LPVOID codecave = a.make();
	if (!codecave) {
		_stprintf(buf, _T("Error making jit function (%u)"), a.getError());
		OutputDebugString(buf);
		return FALSE;
	}

	BYTE* patch = new BYTE[overwrittenBytes];
	if (!patch) {
		MemoryManager::getGlobal()->free(codecave);
		OutputDebugString(_T("Couldn't allocate memory"));
		return FALSE;
	}
	memset(patch, 0x90, overwrittenBytes);

	patch[0] = 0xE9;
	*(DWORD *)((BYTE*)patch + 1) = (DWORD)codecave - addr - 5;

	BOOL ret = WriteProcessMemory(GetCurrentProcess(), (LPVOID)addr, patch, overwrittenBytes, NULL);
	delete patch;
	return ret;
}


bool __stdcall Export10(int code, void** args, int* status) {
	bool ret = true;
	*status = 0;

	static int firstRequestSize = 0;
	static std::string version;
	static char server[256];
	static UINT port;
	static boost::shared_ptr<TSocket> socket_(new TSocket());
	static boost::shared_ptr<TTransport> transport_ (new TFramedTransport(socket_));
	static boost::shared_ptr<TProtocol> protocol_(new TBinaryProtocol(transport_));
	static std::vector<std::string> initInputs = std::vector<std::string>();

	if (code == 0x11) {
		version = std::string((const char *)args[2]);
	} else if (code == 0x4) {
		// ini parsing
		char buf[4096];
		GetModuleFileNameA(NULL, buf, sizeof(buf));
		for (size_t i = strlen(buf); i > 0; i--) {
			if (buf[i] == '\\') {
				strcpy(buf+i+1, "server.ini");
				break;
			}
		}

		GetPrivateProfileStringA("server", "ip", "localhost", server, sizeof(server), buf);
		port = GetPrivateProfileIntA("server", "port", 9090, buf);

		socket_->setHost(server);
		socket_->setPort(port);

		GetPrivateProfileStringA("crc", version.c_str(), NULL, buf, sizeof(buf), buf);

		std::vector<std::string> patchInfos = std::vector<std::string>();
		for (const char* patchInfo = strtok(buf, "|"); patchInfo; patchInfo = strtok(NULL, "|")) {
			OutputDebugStringA(patchInfo);
			patchInfos.push_back(patchInfo);
		}

		BOOL status = TRUE;
		std::for_each(patchInfos.begin(), patchInfos.end(), [&] (std::string patchInfo) {
			strcpy(buf, patchInfo.c_str());

			DWORD addr = 0;
			DWORD addr2;
			auto ranges = std::vector<std::pair<DWORD, DWORD>>();
			auto range = std::pair<DWORD, DWORD>(0, 0);

			for (const char* address = strtok(buf, ","); address; address = strtok(NULL, ",")) {
				if (addr == 0) {
					if (sscanf(address, "%x", &addr) != 1) {
						return;
					}
				} else {
					if (sscanf(address, "%x", &addr2) != 1) {
						return;
					}

					if (range.first == 0) {
						range = std::make_pair(addr2, 0);
					} else {
						range = std::make_pair(range.first, addr2);
						ranges.push_back(range);
						range = std::pair<DWORD, DWORD>(0, 0);
					}
				}
			}

			status = status && PatchCRC(addr, ranges);
		});

		if (!status) {
			MessageBox(NULL, _T("Could not bypass MSCRC"), _T("Bypass failed!"), 0);
		}

		sprintf(buf, "Server %s:%d", server, port);
		OutputDebugStringA(buf);

	} else if (code == 0xd) {
		std::string input((char *)args[0], (int)args[1]);

		if (firstRequestSize == 0) {
			firstRequestSize = input.size();
		}

		if (input.size() == firstRequestSize) {
			initInputs.clear();
		}

		for (int i = 0; i < 3; i++) {
			try {
				HeartbeatClient client(protocol_);
				HeartbeatOutput output;

				if (!transport_->isOpen()) {
					transport_->open();
				}
				client.heartbeat(output, input, initInputs);

				memcpy(args[2], output.output.data(), 0x192);
				*status = output.errorCode;
				ret = output.success;
				break;
			} catch (TException& te) {
				OutputDebugStringA(te.what());
				transport_->close();
			}
		}

		if (initInputs.size() < 2) {
			initInputs.push_back(input);
		}
	}

	// log inputs to export 10
	char buf[256];
	switch (code) {
	case 0x11: // startmonitor
		sprintf(buf, "%x 0:'%s' 1:'%s' 2:'%s' 3:'%s'", code, args[0], args[1], args[2], args[3]);
		break;

	case 0x4: // initialize
		sprintf(buf, "%x 0:%08x 1:%08x 2:'%s' 3: %08x 4:%08x 5:%08x 6:'%s', 7:%08x 8:%08x 9:%08x 10:%08x", code, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10]);
		break;

	case 0x13: // setuserid
		sprintf(buf, "%x 0:'%s'", code, args[0]);
		break;

	case 0xd: // heartbeat
		sprintf(buf, "%x 1:%X => (%x, %d) olen: %x", code, (int)args[1], *status, ret, ((PAHNHS_TRANS_BUFFER)(args[2]))->nLength);
		break;

	case 0x5: // startservice
	case 0x22: // checkhackshieldrunning
	default:
		sprintf(buf, "%x %08X", code, args);
		break;
	}
	OutputDebugStringA(buf);

	return ret;
}


LONG CALLBACK VectoredHandler(PEXCEPTION_POINTERS ExceptionInfo) {
	typedef struct { DWORD unused[2]; char typeName[]; } TypeInfo4;
	typedef struct { DWORD unused[1]; TypeInfo4* info; } TypeInfo3;
	typedef struct { DWORD unused[1]; TypeInfo3* info; } TypeInfo2;
	typedef struct { DWORD unused[3]; TypeInfo2* info; } TypeInfo1;

	if (ExceptionInfo->ExceptionRecord->ExceptionCode == 0xE06D7363) {
		// internal hack detection bypass
		char* name = ((TypeInfo1*)(ExceptionInfo->ExceptionRecord->ExceptionInformation[2]))->info->info->info->typeName;
		if (strstr(name, "SecurityThreatDetected") != NULL) {
			return EXCEPTION_CONTINUE_EXECUTION;
		}
	}

	return EXCEPTION_CONTINUE_SEARCH;
}


DWORD WINAPI ThreadProc(LPVOID) {
	// internal hack detection
	if (AddVectoredExceptionHandler(1, &VectoredHandler) == NULL) {
		MessageBox(0, _T("Couldn't bypass internal hack detection"), _T("Bypass failed"), MB_ICONERROR);
		return 0;
	}

	// memory backup
	HMODULE mod = GetModuleHandle(NULL);
	IMAGE_NT_HEADERS* nth = (IMAGE_NT_HEADERS*)((unsigned int)mod + PIMAGE_DOS_HEADER(mod)->e_lfanew);

	imgStart = (DWORD)mod;
	imgEnd = imgStart + nth->OptionalHeader.SizeOfImage;

	memoryBackup = (unsigned char *)malloc(nth->OptionalHeader.SizeOfImage);
	if (!memoryBackup) {
		MessageBox(0, _T("Couldn't allocate memory backup"), _T("Bypass failed"), MB_ICONERROR);
		return 0;
	}

	memcpy(memoryBackup, (LPVOID)imgStart, nth->OptionalHeader.SizeOfImage);

	return 0;
}


BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
	if (fdwReason == DLL_PROCESS_ATTACH) {
		return (BOOL)CreateThread(NULL, 0, ThreadProc, NULL, 0, NULL);
	}
	return TRUE;
}