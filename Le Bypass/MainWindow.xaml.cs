﻿using System;
using System.Windows;

namespace Le_Bypass
{
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;

    using Microsoft.Win32;

    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key,string val,string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key,string def, StringBuilder retVal,
            int size,string filePath);

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath"></PARAM>
        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void IniWriteValue(string Section,string Key,string Value)
        {
            WritePrivateProfileString(Section,Key,Value,this.path);
        }
        
        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Path"></PARAM>
        /// <returns></returns>
        public string IniReadValue(string Section,string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section,Key,"",temp,255, this.path);
            return temp.ToString();

        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string hshieldPath;
        private readonly string ehsvcPath;
        private readonly string hsupdatePath;
        private readonly string autoupPath;
        private readonly string myPath;

        public MainWindow()
        {
            this.InitializeComponent();

            this.hshieldPath = this.GetHackshieldPath();
            if (string.IsNullOrEmpty(this.hshieldPath))
            {
                MessageBox.Show("Could not find MapleStory folder");
                Application.Current.Shutdown();
                return;
            }

            this.myPath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            if (this.myPath == null)
            {
                MessageBox.Show("Could not find installation folder of Le Bypass");
                Application.Current.Shutdown();
                return;
            }

            this.ehsvcPath = Path.Combine(this.hshieldPath, "ehsvc.dll");
            this.hsupdatePath = Path.Combine(this.hshieldPath, "HSUpdate.exe");
            this.autoupPath = Path.Combine(this.hshieldPath, "Update\\autoup.exe");

            var hsupdateInfo = new FileInfo(this.hsupdatePath);
            if (hsupdateInfo.Exists && hsupdateInfo.Length < 50000)
            {
                this.buttonToggle.Content = "Restore HackShield";
            }
            else
            {
                this.buttonToggle.Content = "Replace HackShield";
            }
        }

        private string GetHackshieldPath()
        {
            var path = Path.Combine((string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Europe MapleStory_is1", "InstallLocation", null) ?? Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "NEXON\\Europe MapleStory\\"), "HShield\\");

            if (!Directory.Exists(path))
            {
                var dialog = new OpenFileDialog
                {
                    Filter = "MapleStory.exe|MapleStory.exe"
                };

                if (dialog.ShowDialog() == true)
                {
                    var maplestoryPath = Path.GetDirectoryName(dialog.FileName);
                    if (maplestoryPath != null)
                    {
                        path = Path.Combine(maplestoryPath, "HShield\\");
                    }
                    return path;
                }

                return string.Empty;
            }

            return path;
        }

        private void buttonToggle_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Please close MapleStory before continuing");

            var hsupdateInfo = new FileInfo(this.hsupdatePath);
            if (hsupdateInfo.Exists && hsupdateInfo.Length < 50000)
            {
                // restore original HSUpdate.exe
                try
                {
                    File.Copy(Path.Combine(this.myPath, "original\\autoup.exe"), this.autoupPath, true);
                    File.Copy(Path.Combine(this.myPath, "original\\HSUpdate.exe"), this.hsupdatePath, true);
                    MessageBox.Show("HackShield has been restored. You can close this program and open MapleStory.", "Success");
                    this.buttonToggle.Content = "Replace HackShield";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Could not restore HackShield");
                }
            }
            else
            {
                // replace Hackshield
                try
                {
                    File.Copy(Path.Combine(this.myPath, "ehsvc.dll"), this.ehsvcPath, true);
                    File.Copy(Path.Combine(this.myPath, "autoup.exe"), this.autoupPath, true);
                    File.Copy(Path.Combine(this.myPath, "HSUpdate.exe"), this.hsupdatePath, true);
                    MessageBox.Show("HackShield is replaced! You can close this program and open MapleStory.", "Success!");
                    this.buttonToggle.Content = "Restore HackShield";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Could not replace HackShield");
                }
            }
        }

        private void buttonCheckServer_Click(object sender, RoutedEventArgs e)
        {
            using (var client = new WebClient())
            {
                client.Proxy = null;
                client.DownloadFile("http://dl.dropbox.com/u/132710081/server.ini", "server.ini");
            }

            var ini = new IniFile(Path.GetFullPath("server.ini"));
            var ip = ini.IniReadValue("server", "ip");
            int port;

            if (!int.TryParse(ini.IniReadValue("server", "port"), out port))
            {
                MessageBox.Show("Could not find server details");
                return;
            }

            try
            {
                using (var tcp = new TcpClient())
                {
                    var ar = tcp.BeginConnect(ip, port, null, null);
                    var wh = ar.AsyncWaitHandle;
                    try
                    {
                        if (!ar.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(3), false))
                        {
                            tcp.Close();
                            throw new TimeoutException();
                        }

                        tcp.EndConnect(ar);
                    }
                    finally
                    {
                        wh.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is TimeoutException || ex is SocketException)
                {
                    MessageBox.Show("Bypass server is down");
                    return;
                }
                throw;
            }

            MessageBox.Show("Bypass server is up!");
        }
    }
}
