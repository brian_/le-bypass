struct HeartbeatOutput {
    1: bool success,
    2: i32 errorCode,
    3: binary output
}

service Heartbeat {
    HeartbeatOutput heartbeat(1: binary input, 2: list<binary> initInputs)
}